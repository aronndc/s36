// Setup imports

const express = require('express')
const mongoose = require('mongoose')
// const dotenv = require('dotenv')

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')

// Express Setup
const app = express()
const port = 3001

// Initialize dotenv
// dotenv.config()

app.use(express.json())
app.use(express.urlencoded({extended: true}))
// Routes
app.use('/tasks', taskRoutes)
// NOTE: Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the 'tasks' collection in MongoDb, then we are specifying an endpoint with specific routes and controllers for that collection.

// Mongoose Setup
mongoose.connect(`mongodb+srv://aronndc:admin123@zuitt-batch-197.lzwxz7u.mongodb.net/S35?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.once('open', () => console.log('Connected to MongoDB!'))





// Listen to port
app.listen(port, () => console.log(`Server is running at port: ${port}`))
	