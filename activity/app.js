const express = require('express');
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes.js');

const app = express();
const port = 3001;


app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/tasks', taskRoutes)
mongoose.connect(`mongodb+srv://aronndc:admin123@zuitt-batch-197.lzwxz7u.mongodb.net/S36?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});


let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'));
db.once('open', () => console.log('Connected to MongoDB!'));



app.listen(port, () => console.log(`Server is running at port: ${port}`));